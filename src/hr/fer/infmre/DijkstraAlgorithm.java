package hr.fer.infmre;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Class representing the Dijkstra Algorithm.
 */
public class DijkstraAlgorithm {

    /**
     * All nodes in the graph.
     */
    private final Set<Node> nodes;
    /**
     * All edges in the graph.
     */
    private final Set<Edge> edges;
    /**
     * Collection of settled nodes.
     */
    private Set<Node> settledNodes;
    /**
     * Collection of unsettled nodes.
     */
    private Set<Node> unsettledNodes;

    /**
     * Construcor for the Dijkstra Algorithm.
     * @param graph graph which algorithm will be used on
     */
    public DijkstraAlgorithm(Graph graph) {
        // create a copy of the array so that we can operate on this array
        this.nodes = graph.getNodes();
        this.edges = graph.getEdges();
    }

    /**
     * Executes algorithm for the given node as the source node.
     * @param source node used as the source node in the algorithm
     */
    public void execute(Node source) {

        int step = 0;
        settledNodes = new HashSet<>();
        unsettledNodes = nodes;
        source.setDistance(0);

        while (unsettledNodes.size() > 0) {
            System.out.println("Step " + step);
            System.out.println("\tSettled: " + settledNodes);
            System.out.println("\tUnsettled: " + unsettledNodes);

            Node node = getMinimum(unsettledNodes);

            System.out.println("\tMinimum node of unsettled: " + node);
            System.out.println(node.getDistance() != Integer.MAX_VALUE ? "\tDistance: " + node.getDistance() : "\tDistance: INF");
            System.out.println(node.getDistance() != Integer.MAX_VALUE ? "\tPredecessors: " + node.getPredecessors() : "\tPath: NOT REACHABLE");

            settledNodes.add(node);
            unsettledNodes.remove(node);
            findMinimalDistances(node);
            step++;
        }
    }

    /**
     * Gets node with the minimum distance from the given set of nodes.
     * @param nodes set of nodes to be checked for the minimum distance node
     * @return minimum distance node
     */
    private Node getMinimum(Set<Node> nodes) {
        Node minimum = null;
        for (Node node : nodes) {
            if (minimum == null) {
                minimum = node;
            } else {
                if (node.getDistance() < minimum.getDistance()) {
                    minimum = node;
                }
            }
        }
        return minimum;
    }

    /**
     * Finds new minimal distances for all nodes that are neighbour to the given node.
     * If neighbour's current distance is bigger than new possible minimal distance then:
     * Distance for neighbour node is calculated as:
     * distance of the given node + the distance between the given node and the neighbour.
     * Then the given node is added to the predecessors list of the neighbour node.
     *
     * If the distance of the given node is Integer.MAX_VALUE (which means it is unreachable),
     * then minimal distances for its neighbours are not calculated (since it means they are not reachable as well).
     *
     * @param node node whose neighbours' new minimal distances are calculated
     */
    private void findMinimalDistances(Node node) {
        if (node.getDistance() == Integer.MAX_VALUE)
            return;
        List<Node> adjacentNodes = getNeighbors(node);
        for (Node neighbour : adjacentNodes) {
            if (neighbour.getDistance() > node.getDistance() + getDistanceBetween(node, neighbour)) {
                neighbour.setDistance(node.getDistance() + getDistanceBetween(node, neighbour));
                LinkedList<Node> shortestPath = new LinkedList<>(node.getPredecessors());
                shortestPath.add(node);
                neighbour.setPredecessors(shortestPath);
            }
        }

    }

    /**
     * Gets all neighbours of the given node.
     *
     * @param node node whose neighbours are searched
     * @return list of neighbour nodes
     */
    private List<Node> getNeighbors(Node node) {
        List<Node> neighbors = new ArrayList<>();
        for (Edge edge : edges) {
            if (edge.getSource().equals(node)
                    && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    /**
     *  Gets the distance between given source and destination nodes (edge weight between them).
     *
     * @param src source node of the searched edge
     * @param destination destination node of the searched edge
     * @return distance (edge weight) between given nodes
     */
    private int getDistanceBetween(Node src, Node destination) {
        for (Edge edge : edges) {
            if (edge.getSource().equals(src) && edge.getDestination().equals(destination)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Node and target are not neighbours.");
    }

    /**
     * Checks if the given node is settled (settled - minimum distance and path are already calculated)
     *
     * @param node checked node
     * @return true if given is settled, false otherwise
     */
    private boolean isSettled(Node node) {
        return settledNodes.contains(node);
    }

}