package hr.fer.infmre;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Class representing data source (nodes, edges and edge weights) for a graph.
 */
public class GraphDataSource {

    private static final String DIRECTED = "u";
    private static final String UNDIRECTED = "n";

    private Set<Edge> edges;
    private Map<String, Node> nodes;

    /**
     * Reads input file and parses it to get nodes, edges and edge weights.
     *
     * @param path path to the input file that contains nodes, edges and edge weights
     * @throws IOException Throws IOException if input file on the given path does not exist.
     */
    public GraphDataSource(Path path) throws IOException {
        edges = new HashSet<>();
        // map is used so we can later get specific node only by its name (id)
        nodes = new HashMap<>();

        BufferedReader reader = Files.newBufferedReader(path);
        String line = reader.readLine();

        while (line != null) {
            String[] lineParts = line.split(", ");

            if (!nodes.containsKey(lineParts[0])) {
                Node firstNode = new Node(lineParts[0]);
                nodes.put(firstNode.getName(), firstNode);
            }
            if (!nodes.containsKey(lineParts[1])) {
                Node secondNode = new Node(lineParts[1]);
                nodes.put(secondNode.getName(), secondNode);
            }

            if (lineParts[3].equals(DIRECTED)) {
                Edge edge = new Edge(nodes.get(lineParts[0]), nodes.get(lineParts[1]), Integer.valueOf(lineParts[2]));
                edges.add(edge);
            } else if (lineParts[3].equals(UNDIRECTED)) {
                try {
                    Edge edge = new Edge(nodes.get(lineParts[0]), nodes.get(lineParts[1]), Integer.valueOf(lineParts[2]));
                    edges.add(edge);
                    Edge secondEdge = new Edge(nodes.get(lineParts[1]), nodes.get(lineParts[0]), Integer.valueOf(lineParts[2]));
                    edges.add(secondEdge);
                } catch (NumberFormatException ex) {
                    throw new IllegalArgumentException("Invalid input file. Edge weight cannot be parsed.");
                }
            } else {
                throw new IllegalArgumentException("Invalid input file. Edge direction (uni- or bi-directional) not correctly specified.");
            }

            line = reader.readLine();
        }

    }

    /**
     * Builds graph based on nodes and edges.
     *
     * @return Graph generated from given nodes and edges.
     */
    public Graph buildGraph() {
        return new Graph(new HashSet<>(nodes.values()), edges);
    }

    /**
     * Finds the node with the given id.
     * If such does not exist, returns null.
     *
     * @param id id of the searched node
     *
     * @return Node with the given id.
     */
    public Node findById (String id) {
        return nodes.get(id);
    }
}
