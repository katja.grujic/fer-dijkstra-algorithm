package hr.fer.infmre;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {

    /**
     * Static path value for input file.
     */
    private static final String PATH = "./demo-graph-data.txt";

    /**
     * Starts the program for given input in ./graph.txt file.
     * Generates graph based on the input file.
     * Program calculates the Dijkstra Algorithm (shortest path) for node entered in the console.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {

        GraphDataSource graphDataSource;
        try {
            graphDataSource = new GraphDataSource(Paths.get(PATH));
        } catch (IOException e) {
            System.out.println("Cannot read input file.");
            return;
        }

        Graph graph = graphDataSource.buildGraph();
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);

        Node sourceNode = null;
        // Dijkstra Algorithm will be calculated for this node.
        while (sourceNode == null) {
            String input = readSourceNode();
            sourceNode = graphDataSource.findById(input);
        }

        dijkstra.execute(sourceNode);
    }

    /**
     * Reads source node from console.
     *
     * @return String representation (id) of source node
     */
    private static String readSourceNode() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter source node ID: ");

        return sc.nextLine();
    }

}
