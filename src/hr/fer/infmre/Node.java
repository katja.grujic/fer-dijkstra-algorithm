package hr.fer.infmre;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Class representing a graph node.
 */
public class Node {

    /**
     * Name (id) of the node.
     */
    private String name;
    /**
     * Distance to the source node.
     * Initialized to Integer.MAX_VALUE to represent infinite distance.
     */
    private int distance = Integer.MAX_VALUE;
    /**
     * List of nodes that precede given node on the path to the source node.
     */
    private List<Node> predecessors = new LinkedList<>();

    public Node(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public List<Node> getPredecessors() {
        return predecessors;
    }

    public void setPredecessors(List<Node> predecessors) {
        this.predecessors = predecessors;
    }

    /**
     * Checks if given node is equal to the other node.
     * Equality is based on node name (id).
     * @param o Object checked for equality
     * @return true if equal, false if not equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Objects.equals(name, node.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }

}
